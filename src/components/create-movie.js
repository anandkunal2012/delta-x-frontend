import React, {Component} from 'react';
import axios from 'axios';
import CreateActor from './create-actor'

const multiSelectStle = {
  'height': '100px',
  'overflowY':'auto'
};

export default class CreateMovie extends Component {
    constructor(props) {
        super(props);

        this.onChangeMoviePlot = this.onChangeMoviePlot.bind(this);
        this.onChangeMovieName = this.onChangeMovieName.bind(this);
        this.onChangeMoviePoster = this.onChangeMoviePoster.bind(this);
        this.onChangeMovieYor = this.onChangeMovieYor.bind(this);
        this.onClickActor = this.onClickActor.bind(this);

        this.onSubmit = this.onSubmit.bind(this);

        this.state = {
            name: '',
            yor: '',
            plot: '',
            poster: '',
            actors:[],
            cast: []
        }
    }
    
    
    handleClose() {
    this.setState({ show: false });
  }

  handleShow() {
    this.setState({ show: true });
  }
    
    updateActors() {
        axios.get('http://localhost:4000/movies/actors')
            .then(response => {
                console.log(response.data)
                this.setState({actors: response.data});
            })
            .catch(function (error) {
                console.log(error);
            })
    }
    componentDidMount() {
        console.log(this.props.match.params.id);
       this.updateActors()

    }


    onChangeMoviePlot(e) {
        this.setState({
            plot: e.target.value
        });
    }
    
    onClickActor(e) {
        console.log(e.target.value)
        var currCast = this.state.cast;
        var arr;
        if(currCast.indexOf(e.target.value) < 0) {
             currCast.push(e.target.value)
        }else {
            currCast.splice(currCast.indexOf(e.target.value),1);
        }
        console.log(currCast);
        
        this.setState({
            cast: currCast
        })
        console.log(this.state)
    }
    onChangeMovieName(e) {
        this.setState({
            name: e.target.value
        });
    }

    onChangeMoviePoster(e) {
        this.setState({
            poster: e.target.value
        });
    }

    onChangeMovieYor(e) {
        this.setState({
            yor: e.target.value
        });
    }

    onSubmit(e) {
        e.preventDefault();

        console.log(`Form submitted:`);
        console.log(`Todo Description: ${this.state.name}`);
        console.log(`Todo Priority: ${this.state.plot}`);
        console.log(`Todo Completed: ${this.state.yor}`);

        const newMovie = {
            name: this.state.name,
            plot: this.state.plot,
            poster: this.state.poster,
            yor: this.state.yor,
            cast: this.state.cast
        }


        axios.post('http://localhost:4000/movies/add', newMovie)
            .then(res => {
                this.props.history.push('/');
            });

        this.setState({
            name: '',
            yor: '',
            plot: '',
            poster: ''
        })
    }
    
    actorList() {
        var that = this;
        return this.state.actors.map(function(currentActor, i) {
            return (
        <div className="form-check" key={i}>
          <input className="form-check-input" type="checkbox" value={currentActor.name} id="defaultCheck1" onClick={that.onClickActor} />
          <label className="form-check-label">
            {currentActor.name}
          </label>
        </div>
                
                )
            // <Actors key={i} actor={currentActor} />
        });
    }

    render() {
        return (
            <div style={{marginTop: 20}}>
                <h3>Create New Movie</h3>
                <form onSubmit={this.onSubmit}>
                    <div className="form-group">
                        <label>Name: </label>
                        <input  type="text"
                                className="form-control"
                                value={this.state.name}
                                onChange={this.onChangeMovieName}
                                />
                    </div>
                    <div className="form-group">
                        <label>yor: </label>
                        <input  type="text"
                                className="form-control"
                                value={this.state.yor}
                                onChange={this.onChangeMovieYor}
                                />
                    </div>
                     <div className="form-group">
                        <label>poster Url: </label>
                        <input  type="text"
                                className="form-control"
                                value={this.state.poster}
                                onChange={this.onChangeMoviePoster}
                                />
                    </div>
                     <div className="form-group">
                        <label>plot: </label>
                        <textarea rows="3" 
                                className="form-control"
                                value={this.state.plot}
                                onChange={this.onChangeMoviePlot}
                                />
                    </div>
                    <div className="form-group">
                        <label>Select Actors</label>
                        <div multiple className="form-control" style={multiSelectStle}>
                          {this.actorList()}
                        </div>

                    </div>
                    <div className="form-group">
                    <CreateActor updateActors={this.updateActors}/> 
                    </div>

                    <div className="form-group">
                        <input type="submit" value="Create Movie" className="btn btn-primary float-right" />
                    </div>
                </form>
                
            </div>
        )
    }
}