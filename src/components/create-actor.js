import React, {Component} from 'react';
import axios from 'axios';

const showModalStyle = {
  display: 'block'
};


export default class CreateActor extends React.Component {

  constructor(props) {
      super(props);

      this.showModal = this.showModal.bind(this);
      this.hideModal = this.hideModal.bind(this);
      this.onChangeActorName = this.onChangeActorName.bind(this);
      this.onChangeActorSex = this.onChangeActorSex.bind(this);
      this.createActor = this.createActor.bind(this);
      this.onChangeActorDob = this.onChangeActorDob.bind(this);
      this.onChangeActorBio = this.onChangeActorBio.bind(this);

      this.state = {
         showModal: false,
         name: '',
         sex: '',
         dob : '',
         bio : ''
      }
  }
  
  createActor() {
    const newActor = {
            name: this.state.name,
            sex: this.state.sex,
            dob: this.state.dob,
            bio: this.state.bio
        }

     axios.post('http://localhost:4000/movies/actor/add', newActor)
            .then(res => console.log(res.data));

    this.hideModal();
  }
  
  onChangeActorName(e) {
        this.setState({
            name: e.target.value
        });
    }

  onChangeActorSex(e) {
        this.setState({
            sex: e.target.value
        });
    }
  onChangeActorDob(e) {
    console.log(e);
        this.setState({
            dob: e.target.value
        });
    }
  onChangeActorBio(e) {
        this.setState({
            bio: e.target.value
        });
    }

  showModal() {
    this.setState({ showModal: true });
  }
  
  hideModal() {
    this.setState({ showModal: false });
  }

  render() {
    return (
      <div>
        <button type="button" className="btn btn-primary" data-toggle="modal" data-target="#exampleModal" onClick={this.showModal}>
          Create Actor
        </button>
    
      { this.state.showModal ? 
        <div className="modal fade show" id="exampleModal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style={showModalStyle}>
          <div className="modal-dialog modal-dialog-centered" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLabel">Create Actor</h5>
                <button type="button" className="close" onClick={this.hideModal} aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className="modal-body">
                <form onSubmit={this.onSubmit} 
                >
                    <div className="form-group">
                        <label>Actor Name: </label>
                        <input  type="text"
                                className="form-control"
                                value={this.state.name}
                                onChange={this.onChangeActorName}
                                />
                    </div>

                    <div className="form-group">
                        <div className="row">
                          <legend className="col-form-label col-sm-2 pt-0">Sex</legend>
                          <div className="form-check form-check-inline">
                            <input className="form-check-input" type="checkbox" id="inlineCheckbox1" value="male" onClick={this.onChangeActorSex} />
                            <label className="form-check-label">Male</label>
                          </div>
                          <div className="form-check form-check-inline">
                            <input className="form-check-input" type="checkbox" id="inlineCheckbox2" value="female" onClick={this.onChangeActorSex}  />
                            <label className="form-check-label">Female</label>
                          </div>
                          <div className="form-check form-check-inline">
                            <input className="form-check-input" type="checkbox" id="inlineCheckbox3" value="others" onClick={this.onChangeActorSex} />
                            <label className="form-check-label">Others</label>
                          </div>
                        </div>
                      </div>

                     <div className="form-group">
                        <label>Date of Birth: </label>
                        <input type="date" id="start" name="trip-start"
                           value={this.state.dob} onChange={this.onChangeActorDob} />
                    </div>

                     <div className="form-group">
                        <label>Bio: </label>
                        <textarea rows="3" 
                                className="form-control"
                                value={this.state.bio}
                                onChange={this.onChangeActorBio}
                                />
                    </div>
                   

                   
                </form>
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-secondary" data-dismiss="modal" onClick={this.hideModal}>Close</button>
                <button type="button" className="btn btn-primary" onClick={this.createActor}>Save changes</button>
              </div>
            </div>
          </div>
        </div>
        : null }
      </div>
    );
  }
}
