import React, {Component} from 'react';
import { Redirect,Link } from 'react-router-dom';

import axios from 'axios';

const Movie = props => (
    <tr>
        <td><img src={props.movie.poster}  width="70"/></td>
        <td>{props.movie.name}</td>
        <td>{props.movie.yor}</td>
        <td>{props.movie.plot}</td>
        <td>{props.movie.cast}</td>
        <td><span className="glyphicon"><a href={/edit/ + props.movie._id}>&#x270f;</a></span></td>
    </tr>
)

export default class MoviesList extends Component {

    constructor(props) {
        super(props);
        this.state = {movies: [], createFlag: false};
    }

    componentDidMount() {
        axios.get('http://localhost:4000/movies/')
            .then(response => {
                this.setState({movies: response.data});
            })
            .catch(function (error) {
                console.log(error);
            })
    }
    createMovie() {
        this.setState({createFlag:true})
    }

    movieList() {
        return this.state.movies.map(function(currentMovie, i) {
            return <Movie movie={currentMovie} key={i} />;
        });
    }

    render() {
        return (
            <div className="mt-3 p-3">
                { this.state.createFlag ? <Redirect to='/create' /> :
                <>
              <h6>
                <span>Top 10 Movies 2018</span>
                <button type="button" className="btn-success float-right" onClick={this.createMovie.bind(this)}>+ Movie</button>
              </h6>

              <table className="table mt-3 table-bordered">
                <thead className="thead-light">
                  <tr>
                    <th scope="col">Poster</th>
                    <th scope="col">Movie Name</th>
                    <th scope="col">Year Of Release</th>
                    <th scope="col">Plot</th>
                    <th scope="col">Cast</th>
                    <th scope="col">Edit</th>
                  </tr>
                </thead>
                <tbody>
                    { this.movieList() }
                </tbody>
              </table>
              </>
              }
            </div>
        )
    }
}