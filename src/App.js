import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";


import CreateMovie from "./components/create-movie";
import EditMovie from "./components/edit-movie";
import MoviesList from "./components/movie-list";

class App extends Component {
  render() {
    return (
      <Router>
        <div>
          
          <nav className="navbar navbar-expand-lg  navbar-dark bg-dark">
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>

            <div className="collapse navbar-collapse" id="navbarSupportedContent">
              <ul className="navbar-nav mr-auto">
                <li className="nav-item active">
                  <a className="nav-link" href="/">Movies <span className="sr-only">(current)</span></a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="#">TV Shows</a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="#">Celebrities</a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="#">Watch List</a>
                </li>
              </ul>
              <form className="form-inline my-2 my-lg-0">
                <input className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" />
              </form>
            </div>
          </nav>
        <div className="container">
          <Route path="/" exact component={MoviesList} />
          <Route path="/edit/:id" component={EditMovie} />
          <Route path="/create" component={CreateMovie} />
        </div>
        </div>
      </Router>
    );
  }
}

export default App;
